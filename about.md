---
layout: page
title: About us
permalink: /about/
---

In the early and mid 1970’s, a group of Swiss soccer players, 
working and residing in Hong Kong, occasionally gathered for 
social games against various local teams.

In 1977 an announcement was made, regarding the intended 
formation of the Yau Yee League, and the Swiss team decided 
to join this fledgling League as one of the founding members. 
The team “Swiss XI” was thus officially founded on 13 July 1977.
