window.onscroll = function () {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 60 || document.documentElement.scrollTop > 60) {
        document.getElementById("nav-wrap").classList.add("scrolled");
    } else {
        document.getElementById("nav-wrap").classList.remove("scrolled");
    }
}