var slides = document.getElementById("slides");
var controls = document.getElementById("controls");
var activeSlide = 0;
var tickCounter = 0;

function move_slideshow(diff) {
    let count = slides.children.length;
    activeSlide = (activeSlide + diff + count) % count;
    update_slideshow();
    tickCounter = 0;
}

function set_slideshow(value) {
    let count = slides.children.length;
    activeSlide = (value - 1 + count) % count;
    update_slideshow();
    tickCounter = 0;
}

function update_slideshow() {
    let count = slides.children.length;
    let currId = activeSlide + 1;
    let prevId = (activeSlide + count - 1) % count + 1;
    let nextId = (activeSlide + 1) % count + 1;

    console.log(slides);
    for (i=0; i<slides.children.length; ++i) {
        if (slides.children[i].classList) {
            slides.children[i].classList.remove("curr");
            slides.children[i].classList.remove("prev");
            slides.children[i].classList.remove("next");
        }
        if (controls.children[i].classList) {
            controls.children[i].classList.remove("curr")
        }
    }

    slides.children[currId-1].classList.add("curr");
    slides.children[prevId-1].classList.add("prev");
    slides.children[nextId-1].classList.add("next");
    controls.children[currId-1].classList.add("curr");
}

set_slideshow(1);
var interval = setInterval(function () {
    if (tickCounter == 4) {
        move_slideshow(1);
    }
    tickCounter = (tickCounter + 1) % 5;
}, 1000);